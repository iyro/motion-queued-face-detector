/*
 * motionQueuedFaceDetector.hpp
 *
 *  Created on: Apr 3, 2015
 *      Author: iyerro
 */

#ifndef MOTIONQUEUEDFACEDETECTOR_HPP_
#define MOTIONQUEUEDFACEDETECTOR_HPP_

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <libgen.h>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>

const std::string VIDEO_INPUT_PATH("/home/iyerro/workspace/CS510_motionQueuedFaceDetector/cs510assignment02data/");
const std::string OUTPUT_PATH("/home/iyerro/workspace/CS510_motionQueuedFaceDetector/Output/Config_");
const std::string CSV_INPUT_PATH("/home/iyerro/workspace/CS510_motionQueuedFaceDetector/cs510assignment02detections_stab.csv");
const std::string CONFIG_METRIC_OUTPUT("/home/iyerro/workspace/CS510_motionQueuedFaceDetector/Output/configMetricOutput.csv");

struct pittpattface {
	std::string Video_id;
	int Frame_id;
	float flt_x;
	float flt_y;
	float frb_x;
	float frb_y;
	float re_x;
	float re_y;
	float le_x;
	float le_y;
};

struct configOutputMetric {
	std::string Video_id;
	std::string Cascade_id;
	int Tot_Fls_Pos;
	int Tot_Pos;
	float Fls_Pos_Rt;
	int Tot_Fls_Neg;
	int Tot_Pot_Neg;
	float Fls_Neg_Rt;
};

std::vector<std::string> getVideoIDList();
std::string remove_extension(const std::string& filename);
void getConfigMetric();

class motionQueuedFaceDetector {
private:
	std::string cascadeFilePath;
	std::string videoFilePath;
	std::string videoID;
	std::string cascadeID;
	std::string baselineCSV;
	std::string cascadeCSV;
	std::string pittPattCSV;
	std::map <int, std::vector <cv::Rect>> pittPattFaces;
	std::map <int, std::vector <cv::Rect>> cascadeFaces;
	std::map <int, int> falsePositives;
	std::map <int, int> falseNegatives;
	cv::CascadeClassifier faceCascade;

	void processFrame (cv::Mat frame, int frameID);
	cv::Mat getMask(int frameID);

public:
	motionQueuedFaceDetector(std::string cascadeFile, std::string videoFile);
	void processVideo ();
};



#endif /* MOTIONQUEUEDFACEDETECTOR_HPP_ */
