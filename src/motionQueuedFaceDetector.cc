/*
 * motionQueuedFaceDetector.cc
 *
 *  Created on: Mar 31, 2015
 *      Author: iyerro
 */

#include "motionQueuedFaceDetector.hpp"

int minNeigh = 1;
float scale = 1.05;
int config = 0;

int main (int argc, char *argv[]) {
	std::stringstream ss;
	ss << "mkdir -p /home/iyerro/workspace/CS510_motionQueuedFaceDetector/Output/";
	system(ss.str().c_str());

	std::ofstream configOut(CONFIG_METRIC_OUTPUT);
	configOut << "Config_ID,scale,minNeigh,Fls_Pos_Rt,Fls_Neg_Rt" << std::endl;
	configOut.close();

	for (;minNeigh<5;minNeigh++) {
		for (;scale<=1.3;scale+=0.05) {
			config++;

			ss.str(std::string());
			ss << "mkdir -p " << OUTPUT_PATH << config << "/";
			system(ss.str().c_str());

			ss.str(std::string());
			ss << OUTPUT_PATH << config << "/output.csv";

			std::ofstream exLog(ss.str().c_str());
			exLog << "Video_id,Cascade_id,Tot_Fls_Pos,Tot_Pos,Fls_Pos_Rt,Tot_Fls_Neg,Tot_Pot_Neg,Fls_Neg_Rt" << std::endl;
			exLog.close();

			std::vector<std::string> videoIDList = getVideoIDList();
			//std::cout << "Vidoe ID List size : " << videoIDList.size() << std::endl;
			for (auto i : videoIDList) {
				std::stringstream ss;
				ss << VIDEO_INPUT_PATH << i << "/" << i << "_rsrtdense_new.avi";
				motionQueuedFaceDetector *m = new motionQueuedFaceDetector(argv[1], ss.str());
				m->processVideo();
				free(m);
			}

			getConfigMetric();
		}
	}
	return 0;
}

void getConfigMetric() {
	std::stringstream ss;
	ss.str(std::string());
	ss << OUTPUT_PATH << config << "/output.csv";
	std::ifstream config(ss.str().c_str());

	float Tot_Fls_Pos=0, Tot_Pos=0, Tot_Fls_Neg=0, Tot_Pot_Neg=0;

	std::string line;

	if (!config.good()) {
		std::cout<<"Unable to Open config csv!" << std::endl;
	}

	std::getline(config, line, '\n');

	while (std::getline(config, line, '\n')) {
		configOutputMetric p;
		std::replace( line.begin(), line.end(), ',', '\t');
		std::istringstream iss (line);
		iss >> p.Video_id;
		iss >> p.Cascade_id;
		iss >> p.Tot_Fls_Pos;
		iss >> p.Tot_Pos;
		iss.precision(6);
		iss.setf(std::ios::fixed);
		iss >> p.Fls_Pos_Rt;
		iss.unsetf(std::ios::fixed);
		iss >> p.Tot_Fls_Neg;
		iss >> p.Tot_Pot_Neg;
		iss.precision(6);
		iss.setf(std::ios::fixed);
		iss >> p.Fls_Neg_Rt;
		iss.unsetf(std::ios::fixed);

		Tot_Fls_Pos += p.Tot_Fls_Pos;
		Tot_Fls_Neg += p.Tot_Fls_Neg;
		Tot_Pos += p.Tot_Pos;
		Tot_Pot_Neg += p.Tot_Pot_Neg;
	}

	config.close();

	std::ofstream configOut(CONFIG_METRIC_OUTPUT, std::ios::app);

	configOut.precision(2);
	configOut.setf(std::ios::fixed);
	configOut << config << "," << scale << "," << minNeigh << ",";
	configOut.unsetf(std::ios::fixed);
	configOut.precision(2);
	configOut.setf(std::ios::fixed);
	configOut << float(Tot_Fls_Pos/Tot_Pos) << "," << float(Tot_Fls_Neg/Tot_Pot_Neg) << std::endl;
	configOut.unsetf(std::ios::fixed);
	configOut.close();
}


motionQueuedFaceDetector::motionQueuedFaceDetector(std::string cascadeFile, std::string videoFile) {
	cascadeFilePath = cascadeFile;
	videoFilePath = videoFile;

	if (!faceCascade.load(cascadeFilePath)) {
		std::cout << "Error loading Cascade Classifier -- " << cascadeFilePath;
		return;
	}

	char *v = new char[videoFilePath.length() + 1];
	strcpy(v, videoFilePath.c_str());
	videoID = remove_extension(basename(v));
	std::string s = "_rsrtdense_new";
	std::string::size_type i = videoID.find(s);
	if (i != std::string::npos)
		videoID.erase(i, s.length());
	delete [] v;

	v = new char[cascadeFilePath.length() + 1];
	strcpy(v, cascadeFilePath.c_str());
	cascadeID = remove_extension(basename(v));
	delete [] v;

	std::stringstream ss;

	ss.str(std::string());
	ss << OUTPUT_PATH << config << "/" << videoID << "/baseline_"<< cascadeID << ".csv";
	baselineCSV = ss.str();

	ss.str(std::string());
	ss << OUTPUT_PATH << config << "/" << videoID << "/" << cascadeID << ".csv";
	cascadeCSV = ss.str();

	ss.str(std::string());
	ss << OUTPUT_PATH << config << "/" << videoID << "/PittPatt" << ".csv";
	pittPattCSV = ss.str();

	ss.str(std::string());
	ss << "mkdir -p " << OUTPUT_PATH << config << "/" << videoID << "/" << cascadeID;
	system(ss.str().c_str());

	ss.str(std::string());
	ss << "mkdir -p " << OUTPUT_PATH << config << "/" << videoID << "/" << "PittPatt";
	system(ss.str().c_str());

	ss.str(std::string());
	ss << "mkdir -p " << OUTPUT_PATH << config << "/" << videoID << "/" << cascadeID << "vsPittPatt";
	system(ss.str().c_str());

	std::ofstream baseline(baselineCSV);
	std::ofstream pittPattOutput(pittPattCSV);
	std::ofstream cascadeOutput(cascadeCSV);

	baseline << "Video_id,Frame_id,No_of_Faces,No_of_Dtctd_Faces,False_pos,False_Neg" << std::endl;
	pittPattOutput << "Video_id,Frame_id,Frame_file,fcen_x,fcen_y,fwidth,fheight" << std::endl;
	cascadeOutput << "Video_id,Frame_id,Frame_file,fcen_x,fcen_y,fwidth,fheight" << std::endl;

	baseline.close();
	pittPattOutput.close();
	cascadeOutput.close();

	std::string line;
	std::ifstream pittpatt(CSV_INPUT_PATH);

	if (!pittpatt.good()) {
		std::cout<<"Unable to Open PittPatt csv!";
		return;
	}

	std::getline(pittpatt, line, '\n');

	while (std::getline(pittpatt, line, '\n')) {
		pittpattface p;
		std::replace( line.begin(), line.end(), ',', '\t');
		std::istringstream iss (line);
		iss >> p.Video_id;
		iss >> p.Frame_id;
		iss.precision(6);
		iss.setf(std::ios::fixed);
		iss >> p.re_x;
		iss >> p.re_y;
		iss >> p.le_x;
		iss >> p.le_y;
		iss >> p.flt_x;
		iss >> p.flt_y;
		iss >> p.frb_x;
		iss >> p.frb_y;
		iss.unsetf(std::ios::fixed);

		if (p.Video_id == videoID) {
			cv::Rect face(floor(p.flt_x), floor(p.flt_y), floor(p.frb_x) - floor(p.flt_x) , floor(p.frb_y) - floor(p.flt_y));
			pittPattFaces[p.Frame_id].push_back(face);
		}
	}

	pittpatt.close();
}

void motionQueuedFaceDetector::processVideo () {
	CvCapture* capture;
	cv::Mat frame;
	int frameCount = 0;

	std::stringstream ss;
	ss << OUTPUT_PATH << config << "/" << "output.csv";

	std::ofstream exLog(ss.str().c_str(), std::ios::app);

	exLog << videoID << "," << cascadeID << ",";

	capture = cvCaptureFromFile(videoFilePath.c_str());

	if (capture) {
		while (true) {
			frame = cvQueryFrame(capture);

			if (!frame.empty()) {
				frameCount++;
				if (pittPattFaces[frameCount].size() > 0)
					processFrame(frame, frameCount);
			} else {
				std::cout << "Video " << videoID << " Processed!" << std::endl;
				break;
			}

			int c = cv::waitKey(10);
			if ((char) c == 'c') {
				break;
			}
		}
		float sumfp=0, sump=0;
		float fpr = 0;
		for (auto i : falsePositives) {
			sumfp += i.second;
			sump += cascadeFaces[i.first].size();
		}

		fpr = sumfp/sump;
		exLog << sumfp << ",";
		exLog << sump << ",";
		exLog.precision(6);
		exLog.setf(std::ios::fixed);
		exLog << fpr << ",";
		exLog.unsetf(std::ios::fixed);

		sumfp = 0;
		sump = 0;
		fpr = 0;

		for (auto i : falseNegatives) {
			sumfp += i.second;
			sump += pittPattFaces[i.first].size();
		}

		fpr = sumfp/sump;
		exLog << sumfp << ",";
		exLog << sump << ",";
		exLog.precision(6);
		exLog.setf(std::ios::fixed);
		exLog << fpr << std::endl;
		exLog.unsetf(std::ios::fixed);
	}
	exLog.close();
	return;
}

void motionQueuedFaceDetector::processFrame (cv::Mat frame, int frameID) {
	float fp = 0;
	float fn = 0;

	std::ofstream cascadeOutput(cascadeCSV, std::ios::app);
	std::ofstream pittPattOutput(pittPattCSV, std::ios::app);
	std::ofstream baseline(baselineCSV, std::ios::app);

	std::cout << "Video ID : " << videoID << std::endl;
	baseline << videoID << ",";
	std::cout << "Frame ID : " << frameID << std::endl;
	baseline << frameID << ",";

	cv::Mat mask = getMask(frameID);

	//cv::imshow("Test", mask);

	std::vector<cv::Rect> faces;
	cv::Mat frame_gray, frame_cascade, frame_cascade_masked, frame_pittpatt;

	std::stringstream pittpattFrame;
	pittpattFrame << OUTPUT_PATH << config << "/" << videoID << "/PittPatt/Frame_" << frameID << ".jpg";

	std::stringstream cascadeFrame;
	cascadeFrame << OUTPUT_PATH << config << "/" << videoID << "/" << cascadeID << "/Frame_" << frameID << ".jpg";

	std::stringstream compareFrame;
	compareFrame << OUTPUT_PATH << config << "/" << videoID << "/" << cascadeID << "vsPittPatt/Frame_" << frameID << ".jpg";

	frame.copyTo(frame_cascade_masked, mask);
	cv::imshow("Test1", frame);
	cv::imshow("Test", frame_cascade_masked);
	frame.copyTo(frame_cascade);
	frame.copyTo(frame_pittpatt);

	cvtColor(frame_cascade_masked, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	faceCascade.detectMultiScale(frame_gray, faces, scale, minNeigh, 0 | CV_HAAR_SCALE_IMAGE, cv::Size(20, 20));

	for (size_t i = 0; i < faces.size(); i++) {
		cascadeFaces[frameID].push_back(faces[i]);
		cv::Point center(faces[i].x + (faces[i].width * 0.5), faces[i].y + (faces[i].height * 0.5));
		cv::ellipse(frame_cascade, center, cv::Size(faces[i].width * 0.5, faces[i].height * 0.5), 0, 0, 360, cv::Scalar(255, 0, 0), 4, 8, 0);
		cv::ellipse(frame, center, cv::Size(faces[i].width * 0.5, faces[i].height * 0.5), 0, 0, 360, cv::Scalar(255, 0, 0), 4, 8, 0);
		cascadeOutput << videoID << "," << frameID << "," << cascadeID << "/Frame_" << frameID << ".jpg" << "," << ceil(faces[i].x + (faces[i].width * 0.5)) << "," << ceil(faces[i].y + (faces[i].height * 0.5)) << "," << faces[i].width << "," << faces[i].height << std::endl;
	}

	fp = faces.size();
	fn = pittPattFaces[frameID].size();

	std::cout << "No. of PittPatt faces : " << fn << std::endl;
	baseline << fn << ",";
	std::cout << "No. of " << cascadeID << " faces : " << fp << std::endl << std::endl;
	baseline << fp << ",";

	for (size_t i = 0; i < pittPattFaces[frameID].size(); i++) {
		bool found = false;
		std::cout << "Pitt Patt face " << i+1 << " fcen_x : " << pittPattFaces[frameID][i].x + ((pittPattFaces[frameID][i].width)/2) << std::endl;
		std::cout << "Pitt Patt face " << i+1 << " fcen_y : " << pittPattFaces[frameID][i].y + ((pittPattFaces[frameID][i].height)/2) << std::endl;
		std::cout << "Pitt Patt face " << i+1 << " face_w : " << pittPattFaces[frameID][i].width << std::endl;
		std::cout << "Pitt Patt face " << i+1 << " face_h : " << pittPattFaces[frameID][i].height << std::endl << std::endl;

		cv::Point center(pittPattFaces[frameID][i].x + (pittPattFaces[frameID][i].width * 0.5), pittPattFaces[frameID][i].y + (pittPattFaces[frameID][i].height * 0.5));
		cv::ellipse(frame_pittpatt, center, cv::Size(pittPattFaces[frameID][i].width * 0.5, pittPattFaces[frameID][i].height * 0.5), 0, 0, 360, cv::Scalar(0, 255, 0), 4, 8, 0);
		cv::ellipse(frame, center, cv::Size(pittPattFaces[frameID][i].width * 0.5, pittPattFaces[frameID][i].height * 0.5), 0, 0, 360, cv::Scalar(0, 255, 0), 4, 8, 0);

		for (size_t j = 0; j<faces.size(); j++) {
			if ((faces[j] & pittPattFaces[frameID][i]).area() >= ((faces[j].area() > pittPattFaces[frameID][i].area())?(pittPattFaces[frameID][i].area()/2):(faces[j].area()/2))) {	//TODO: figure out smaller rect and use that for comparison
				fp--;
				found = true;
				std::cout << "Corresponding " << cascadeID << " face fcen_x : " << faces[j].x + ((faces[j].width)/2) << std::endl;
				std::cout << "Corresponding " << cascadeID << " face fcen_y : " << faces[j].y + ((faces[j].height)/2) << std::endl;
				std::cout << "Corresponding " << cascadeID << " face face_w : " << faces[j].width << std::endl;
				std::cout << "Corresponding " << cascadeID << " face face_h : " << faces[j].height << std::endl << std::endl;
				break;
			}
		}

		if (found) {
			fn--;
		}
		else {
			std::cout << "No corresponding " << cascadeID << " face found!" << std::endl << std::endl;
		}
		pittPattOutput << videoID << "," << frameID << "," << "PittPatt/Frame_" << frameID << ".jpg" << "," << ceil(pittPattFaces[frameID][i].x + (pittPattFaces[frameID][i].width * 0.5)) << "," << ceil(pittPattFaces[frameID][i].y + (pittPattFaces[frameID][i].height * 0.5)) << "," << pittPattFaces[frameID][i].width << "," << pittPattFaces[frameID][i].height << std::endl;
	}

	std::cout << "False Positives : " << fp << std::endl;
	baseline << fp << ",";
	std::cout << "False Negatives : " << fn << std::endl << std::endl;
	std::cout << "########################################################" << std::endl << std::endl;
	baseline << fn << std::endl;

	falsePositives[frameID] = fp;
	falseNegatives[frameID] = fn;

	baseline.close();
	cascadeOutput.close();
	pittPattOutput.close();

	//cv::imshow("Test",frame_cascade);
	cv::imwrite(cascadeFrame.str(), frame_cascade);
	cv::imwrite(pittpattFrame.str(), frame_pittpatt);
	cv::imwrite(compareFrame.str(), frame);
}

cv::Mat motionQueuedFaceDetector::getMask(int frameID) {
	std::stringstream ss;
	ss << VIDEO_INPUT_PATH << videoID << "/" << videoID << "_rsrtdense_new/" << frameID-1 << ".png";
	cv::Mat mask = cv::imread(ss.str(), CV_LOAD_IMAGE_GRAYSCALE);
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours( mask, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
	mask = cv::Mat::zeros(mask.rows, mask.cols, CV_8UC1);
	int rowtl = mask.rows, coltl = mask.cols, rowbr = 0, colbr = 0;
	for (auto i : contours) {
		for (auto j : i) {
			if (j.x < rowtl) {
				rowtl = j.x;
			}
			if (j.y < coltl) {
				coltl = j.y;
			}
			if (j.x > rowbr) {
				rowbr = j.x;
			}
			if (j.y > colbr) {
				colbr = j.y;
			}
		}
	}

	cv::rectangle(mask, cv::Rect(rowtl, coltl, rowbr-rowtl, colbr-coltl), cv::Scalar(255, 255, 255), CV_FILLED);

	return mask;
}

std::vector<std::string> getVideoIDList() {
	std::vector<std::string> videoIDList;

	std::string line;
	std::ifstream pittpatt(CSV_INPUT_PATH);

	if (!pittpatt.good()) {
		std::cout<<"Unable to Open PittPatt csv!" << std::endl;
	}

	std::getline(pittpatt, line, '\n');

	while (std::getline(pittpatt, line, '\n')) {
		pittpattface p;
		std::replace( line.begin(), line.end(), ',', '\t');
		std::istringstream iss (line);
		iss >> p.Video_id;
		iss >> p.Frame_id;
		iss.precision(6);
		iss.setf(std::ios::fixed);
		iss >> p.re_x;
		iss >> p.re_y;
		iss >> p.le_x;
		iss >> p.le_y;
		iss >> p.flt_x;
		iss >> p.flt_y;
		iss >> p.frb_x;
		iss >> p.frb_y;
		iss.unsetf(std::ios::fixed);

		if (!(std::find(videoIDList.begin(), videoIDList.end(), p.Video_id) != videoIDList.end())) {
			videoIDList.push_back(p.Video_id);
		}
	}

	pittpatt.close();

	return videoIDList;
}

std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(".");
    if (lastdot == std::string::npos) return filename;
    return filename.substr(0, lastdot);
}


